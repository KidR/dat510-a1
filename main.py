"""DAT510 main module for the assignment A1."""

from prettytable import PrettyTable
from lib.assignment import part1, part2
import argparse

PROMPT_CONTINUE = "> Press <ENTER> to continue...\n"
PROMPT_CHOICE = "> Select a task by its ID: "

def __parser() -> argparse.Namespace:
    parser = argparse.ArgumentParser(description='Process assignment A1 commands.')
    parser.add_argument("--task",type=str)
    parser.add_argument("--subtask",type=str)
    return parser.parse_args()

def menu():
    """
    Display the interactive menu.

    Menu displayed show the different tasks according to the assignement.
    """
    displayer = PrettyTable()
    displayer.field_names = ["Part 1 ID", "Poly-alphabetic Cipher", "Part 2 ID", "Simplified DES"]
    displayer.add_rows([
        ["a1", "Task 1", "b1", "Task 1"],
        ["a2", "Task 2","b2", "Task 2"],
        ["a3", "Task 3","b3", "Task 3"],
        ["-", "-","b4", "Task 4"],
    ])
    print(displayer)
    return displayer

MENU_CHOICES = {"a1": part1.task_1, "a2": part1.task_2, "a3": part1.task_3, "b1": part2.task_1, "b2": part2.task_2, "b3": part2.task_3, "b4": part2.task_4}

if __name__ == "__main__":
    parser = __parser()
    interactive_mode = True
    while interactive_mode:
        menu()
        if not parser.task:
            choice = input(PROMPT_CHOICE).lower() 
        else:
            choice = parser.task
            interactive_mode = False   
        task = MENU_CHOICES.get(choice,None)
        if task:
            task(parser.subtask)
        else:
            exit(0)
        input(PROMPT_CONTINUE)
