import pytest
from lib import Keygen, SDES , TripleSDES

def test_p10():
    test = "1010000010"
    assert Keygen.p10(test) == "1000001100"

def test_ls1():
    test = "1000001100"
    assert Keygen.ls1(test) == "0000111000"

def test_p8():
    test = "0000111000"
    assert Keygen.p8(test) == "10100100"

def test_ls2():
    test = "0000111000"
    assert Keygen.ls2(test) == "0010000011"

def test_sdes_keys():
    test = "1010000010"
    assert Keygen.sdes_keys(test) == ("10100100","01000011")

def test_sdes_ip():
    test = "10100100"
    assert  SDES.ip(test) == "01110000"

def test_sdes_inv_ip():
    test = "01110000"
    assert  SDES.inv_ip(test) == "10100100"

def test_sdes_inv_ip_of_ip():
    test = "01110000"
    assert  SDES.inv_ip(SDES.ip(test)) == test

def test_sdes_fmapping():
    test = "1101"
    keytest = "10100100"
    assert SDES.fmapping(test,keytest) == "1111"

def test_fk():
    test = "10111101"
    assert SDES.fk("10100100",test[:4],test[4:]) == ("0100","1101")

def test_sdes_sw():
    test = "10100100"
    assert SDES.sw(test) == "01001010"

def test_sdes_encrypt_1():
    test = "10101010"
    keytest = "0000000000"
    assert SDES.encrypt(keytest,test) == "00010001"

def test_sdes_encrypt_2():
    test = "10101010"
    keytest = "1110001110"
    assert SDES.encrypt(keytest,test) == "11001010"

def test_sdes_encrypt_3():
    test = "01010101"
    keytest = "1110001110"
    assert SDES.encrypt(keytest,test) == "01110000"

def test_sdes_encrypt_4():
    test = "10101010"
    keytest = "1111111111"
    assert SDES.encrypt(keytest,test) == "00000100"

def test_sdes_decrypt_1():
    test = "00010001"
    keytest = "0000000000"
    assert SDES.decrypt(keytest,test) == "10101010"
    

def test_sdes_decrypt_2():
    test = "11001010"
    keytest = "1110001110"
    assert SDES.decrypt(keytest,test) == "10101010"
    

def test_sdes_decrypt_3():
    test = "01110000"
    keytest = "1110001110"
    assert SDES.decrypt(keytest,test) == "01010101"
    

def test_sdes_decrypt_4():
    test = "00000100"
    keytest = "1111111111"
    assert SDES.decrypt(keytest,test) == "10101010"
    
def test_triplesdes_encrypt_1():
    test = "00000000"
    k1test = "1111111111"
    k2test = "1111111111"
    assert TripleSDES.encrypt(k1test,k2test,test) == "11101011"

def test_triplesdes_decrypt_1():
    test = "11101011"
    k1test = "1111111111"
    k2test = "1111111111"
    assert TripleSDES.decrypt(k1test,k2test,test) == "00000000"

def test_0():
    test = "simplifieddesisnotsecureenoughtoprovideyousufficientsecurity"
    bits = ["{:0>8b}".format(ord(c)) for c in test]
    encrypted = [SDES.encrypt("1111101010",b) for b in bits]
    assert "".join(encrypted) == "010001110000000101000000110011011100101100000001011101000000000101101110010101110101011101101110010001110000000101000111101110100100111110001000010001110110111001001100101011111001011101101110011011101011101001001111101011110000100101001010100010000100111111001101100101110100111100110010000000010101011101101110100100000100111110101111010001111010111101110100011101000000000101001100000000010110111010111010100010000100011101101110010011001010111110010111000000011000100010010000"

