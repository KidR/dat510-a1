# DAT510 Assignment A1

**Author: @RainuiLy**

```
.
├── data
│   ├── part1
│   │   ├── en_bigrams.csv
│   │   ├── en_monograms.csv
│   │   ├── en_quadgrams.csv
│   │   ├── en_trigrams.csv
│   │   ├── english_raw_text.txt
│   │   ├── task1.txt
│   │   └── task3.txt
│   └── part2
│       ├── ctx1.txt
│       ├── ctx2.txt
│       ├── task1.csv
│       └── task2.csv
├── docs
├── lib
│   ├── __init__.py
│   ├── assignment
│   │   ├── __init__.py
│   │   ├── part1.py
│   │   └── part2.py
│   └── classes
│       ├── __init__.py
│       ├── exectimer.py
│       ├── polyalphabetic.py
│       └── sdes.py
├── main.py
├── requirements.txt
├── rsc
├── server.conf
├── server.py
└── tests
    ├── __init__.py
    └── test_sdes.py
```

## Getting started

This guideline is written for MacOS X and Linux user only.

### Pre-requisites

This assignment has been realised in Python 3 programming.

- Python >=3.9
- pip >=21.2.4

### Installation

Clone the git project to your computer

```shell
git clone https://gitlab.com/KidR/dat510-a1.git
```

and change the current directory to the project root

```shell
cd dat510-a1/
```

#### Using global environment of your computer

If you don't mind to install the dependencies in your global Python environment, please do :

```shell
python3 -m pip install -r requirements.txt
```

Otherwise, please follow the section below.

#### Using virtualenv

1. Install virtualenv to preserve the Python global environment on your computer of the dependencies used. 

```shell
python3 -m pip install virtualenv
```

2. Create a virtualenv 

```shell
virtualenv -p $(which python3) venv
```

3. Activate the virtualenv to run python file

```shell
source venv/bin/activate
```

4. Verify the virtualenv

```shell
python --version
```

5. Install the depencies required for the assignment

```shell
python -m pip install -r requirements.txt
```

6. To deactivate the virtualenv after testing the project source, please do :

```shell
deactivate
```

### Run the project

The project contains one main file which is `main.py` that run the functionalities to solve the assignment.

To run the source code:

```shell
python main.py
```

*Note: If you are using virtualenv and run the source code for the first time, it will take a few moment to run in order to create virtualenv cache.* 

#### Interactive code

##### Main menu

When the `main.py` is running, an interactive prompt will appear. It allows you to select which task you want to execute according to the assignment.

```
+-----------+------------------------+-----------+----------------+
| Part 1 ID | Poly-alphabetic Cipher | Part 2 ID | Simplified DES |
+-----------+------------------------+-----------+----------------+
|     a1    |         Task 1         |     b1    |     Task 1     |
|     a2    |         Task 2         |     b2    |     Task 2     |
|     a3    |         Task 3         |     b3    |     Task 3     |
|     -     |           -            |     b4    |     Task 4     |
+-----------+------------------------+-----------+----------------+
> Select a task by its ID: 
```

For instance, if you want to execute the task 2 of the **Simplified DES** part, you have to type `b2` and press `<ENTER>`.
To quit this interactive shell properly, you can type `q` or anything that is not part of the ID within the table.

##### Task successfully executed

```
> Press <ENTER> to continue...
```

When a task is successfully done, this line will appear on the terminal. Otherwise, it means that the task is still running. Especially for the **Task 3 of Simplified DES `b3`**, it will take *7 min in average* to be completed.

### Run the library test

Tests are written in native Python with the `pytest` framework.

Run the tests:

```shell
pytest
```