"""Flask server for testing SDES and TripleSDES cipher."""
from flask import Flask, request
from lib import SDES, TripleSDES, Utils
import json

app = Flask(__name__)
CONF = "server.conf"

@app.route("/")
def home():
    """Homepage.

    Returns:
        (str): homepage
    """
    return "It works!"

@app.route("/sdes")
def sdes():
    """
    SDES rest resource.

    GET Args:
        decrypt: decipher a SDES 8-bits blocks
        encrypt: decipher a SDES plaintext
    """
    if request.args.get("decrypt"):
        with open(CONF, "r") as fp:
            keys = json.load(fp)
            data = request.args.get("decrypt")
            text = "".join(
                [
                    Utils.binary_to_char(
                        SDES.decrypt(keys["k1"], data[8 * i : 8 * (i + 1)])
                    )
                    for i in range(len(data) // 8)
                ]
            )
            return f"<p>{text}</p>"
    if request.args.get("encrypt"):
        with open(CONF, "r") as fp:
            keys = json.load(fp)
            blocks = "".join(
                [
                    SDES.encrypt(keys["k1"], Utils.binary(ord(c), 8))
                    for c in request.args.get("encrypt")
                ]
            )
            return f"<p>{blocks}</p>"
    return f"<p>SDES</p>"


@app.route("/triplesdes")
def triple_sdes():
    """
    Triple SDES rest resource.

    GET Args:
        decrypt: decipher a TripleSDES 8-bits blocks
        encrypt: decipher a TripleSDES plaintext
    """
    if request.args.get("decrypt"):
        with open(CONF, "r") as fp:
            keys = json.load(fp)
            data = request.args.get("decrypt")
            text = "".join(
                [
                    Utils.binary_to_char(
                        TripleSDES.decrypt(
                            keys["k1"], keys["k2"], data[8 * i : 8 * (i + 1)]
                        )
                    )
                    for i in range(len(data) // 8)
                ]
            )
            return f"{text}"
    if request.args.get("encrypt"):
        with open(CONF, "r") as fp:
            keys = json.load(fp)
            blocks = "".join(
                [
                    TripleSDES.encrypt(keys["k1"], keys["k2"], Utils.binary(ord(c), 8))
                    for c in request.args.get("encrypt")
                ]
            )
            return f"{blocks}"
    return f"<p>TripleSDES</p>"
