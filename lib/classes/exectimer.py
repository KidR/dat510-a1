"""Module for recording the time elapsed for a function."""
from timeit import default_timer


def time_elapsed(func):
    """
    Calculate the process time of a function's execution.

    Use this function as a Python decorator.

    Args:
        func (function): the function to execute and process the time elapsed

    Returns:
        function: function to run
    """
    def inner(*args):
        s = default_timer()
        func()
        e = default_timer()
        print(f"Time elapsed: {e - s}s")
    return inner
