"""Module for Polyalphabetic cipher.

This module contains a lot of useful methods to analyse
and de/cipher the part 1 of the assignment
"""
from typing import Tuple, Union
import math


def load_datagrams(path: str, as_tuple: bool = True) -> dict:
    """
    Load datagrams from {path}.

    Args:
        path (str): path of datagram to load
        as_tuple (bool, optional): Type of return is tuple if True else dict

    Returns:
        dict | tuple: loaded grams
    """
    grams = {}
    with open(path, "r") as fp:
        for line in fp:
            line = line.split()
            grams[line[0]] = int(line[1])
        if as_tuple:
            grams = list(grams.items())
    return grams


class CryptanalysisFunc:
    """Cryptanalysis class which contains useful function to analyse a cipher text."""

    EN_1_GRAMS = load_datagrams("data/part1/en_monograms.csv", as_tuple=False)
    EN_2_GRAMS = load_datagrams("data/part1/en_bigrams.csv", as_tuple=False)
    EN_3_GRAMS = load_datagrams("data/part1/en_trigrams.csv", as_tuple=False)
    EN_4_GRAMS = load_datagrams("data/part1/en_quadgrams.csv", as_tuple=False)

    @staticmethod
    def frequency(text: str, subtext: str) -> int:
        """
        Calculate the frequency of {subtext} in a {text}.

        Args:
            text (str): text
            subtext (str): subtext to count inside the text

        Returns:
            int: number of occurence of {subtext} in {text}
        """
        return text.count(subtext)

    @staticmethod
    def relative_frequency(
        frequency: int, total_count: int, precision: int = 1
    ) -> float:
        """
        Calculate the relative frequency.

        Args:
            frequency (int): frequency
            total_count (int): total of count of the given frequency
            precision (int, optional): precision of the relative frequency decimals.

        Returns:
            float: relative frequency
        """
        return round((frequency * 100) / total_count, precision)

    @staticmethod
    def fitness(
        frequency: Union[str, int],
        text: Union[str, int],
        gram_length: int = 0,
        precision: int = 1,
    ) -> float:
        """
        Calculate {gram_length} statistic as a fitness mesure of a {text}.

        Args:
            frequency (str | int): either subtext or frequency of a subtext
            text (str | int): either text if frequency is str or text length if frequency is int
            gram_length (int): 0 if frequency is str else length of subtext of the given frequency
            precision (int): precision of the result

        Returns:
            float: fitness
        """
        if isinstance(frequency, int) and isinstance(text, int) and gram_length > 0:
            return round(math.log(frequency / (text - gram_length + 1)), precision)
        if isinstance(frequency, str) and isinstance(text, str):
            return round(
                math.log(
                    CryptanalysisFunc.frequency(text, frequency)
                    / (len(text) - len(frequency) + 1)
                ),
                precision,
            )
        raise ValueError("***** Parameters are incorrect.")

    @staticmethod
    def fitness_in_english(gram: str) -> float:
        """
        Calculate the the {gram} statistic fitness in English.

        Args:
            gram (str): gram to observe

        Returns:
            float: fitness
        """
        en_grams = getattr(CryptanalysisFunc, f"EN_{len(gram)}_GRAMS")
        return math.log(en_grams.get(gram, 1) / len(en_grams))

    @staticmethod
    def attack_vigenere_by_fitness(
        gram: str, text: str, key_length: int, gram_position: int = 0
    ) -> Tuple[float, str]:
        """
        Attack a given {text} by calculate the fitness of a {gram} at the {gram_position} from the beginning of the text.

        It calculate the fitness of the {gram} and

        Args:
            gram (str): gram which is a part of the key
            text (str): text to attack
            key_length (int): length of the cipher key
            gram_position (int, optional): position of the {gram} from the beginning of the text

        Returns:
            Tuple[float,str,str]: fitness of the text and the gram
        """
        fitness = 0
        deciphered_gram = gram
        text = text[gram_position:]
        for i in range(len(text) // key_length):
            subtext = (text[i * key_length : key_length * (i + 1)])[: len(gram)]
            deciphered_gram = CryptanalysisFunc.vigenere_shift(
                subtext, deciphered_gram, reverse=True
            )
            fitness += CryptanalysisFunc.fitness_in_english(deciphered_gram)
        return fitness, gram

    @staticmethod
    def caesar_shift(text: str, shiftchar: str, reverse: bool = False) -> str:
        """
        Shift each letters of the given {text} by the given letter {shiftchar} in ASCII table \
        and process the frequencies of the new text.

        Args:
            text (str): text to caesar shift
            shiftchar (str): use the position of this letter in ASCII table to shift
            reverse (bool): apply left shift (-) if True else right shift (+)

        Returns:
            str: the shifted text by Caesar
        """
        shiftchar_ord = -1 * ord(shiftchar) if reverse else ord(shiftchar)
        return "".join(
            [
                chr(((ord(char) - ord("A") + shiftchar_ord + ord("A")) % 26) + ord("A"))
                for char in text
            ]
        )

    @staticmethod
    def vigenere_shift(
        text: str, key: str, reverse: bool = False, autokey: bool = False
    ) -> str:
        """
        Apply the Vigenere shift/cipher on a given {text} according to the {key}.

        It is possible to apply a right shift or a left shit.
        By default, the {key} is repeated until key has the same length of the {text}.
        It is possible to use an autokey system instead of a repeated key.

        Args:
            text (str): text to apply Vigenere
            key (str): key of Vigenere
            reverse (bool, optional): apply left shift (-) if True else right shift (+)
            autokey (bool, optional): apply autokey system if True else repeated key

        Returns:
            str: the shifted text by Vigenere
        """
        deciphered = []
        text = text.upper()
        key_length = len(key)
        text_length = len(text)
        if autokey:
            nkey = key
            for i in range(text_length // key_length):
                nkey = CryptanalysisFunc.vigenere_shift(
                    text[i * key_length : (i + 1) * key_length], nkey, reverse=True
                )
                deciphered.append(nkey)
        else:
            repeated_key = (key + (key * (text_length - key_length)))[:text_length]
            deciphered = [
                CryptanalysisFunc.caesar_shift(c, k, reverse=reverse)
                for c, k in zip(text, repeated_key)
            ]
            text = "".join(deciphered)
        return "".join(deciphered)


class TextAnalyzer:
    """TextAnalyzer class to analyze a text by using cryptanalysis functions."""

    INDEX_FREQ = 2
    INDEX_RELATIVE_FREQ = 1
    INDEX_GRAM = 0
    INDEX_FITNESS = 3
    EN_FREQS_1 = load_datagrams("data/part1/en_monograms.csv")
    EN_FREQS_2 = load_datagrams("data/part1/en_bigrams.csv")
    EN_FREQS_3 = load_datagrams("data/part1/en_trigrams.csv")
    EN_FREQS_4 = load_datagrams("data/part1/en_quadgrams.csv")
    IC_ENGLISH = 0.0667

    def __init__(self, text: str, gram_length: int = 1):
        """
        Initialize a TextAnalyzer with the given {text} and set the gram length \
            for the statistical frequency at {gram_length}.

        Args:
            text (str): text to analyze
            gram_lengt (int, optional): gram length for statistical purpose
        """
        self.__text = text
        self.__dict_matches = {}
        self.__freqs = {}
        self.__most_used_grams = []
        self.__gram_length = gram_length
        self.__fitness = 0

    @property
    def text(self) -> str:
        """
        Get text.

        Returns:
            str: text
        """
        return self.__text

    @property
    def freqs(self) -> dict:
        """
        Get dict of the frequencies of the given text.

        Returns:
            dict: frequencies
        """
        return self.__freqs

    @property
    def dict_matches(self) -> dict:
        """
        Get dict of matches gram with the best association of the english gram \
            according to the frequency analysis.

        Returns:
            dict: gram matches
        """
        return self.__dict_matches

    @property
    def most_used_grams(self) -> list:
        """
        Get most used grams in the given text.

        Returns:
            list: most used grams
        """
        return self.__most_used_grams

    @property
    def length(self) -> int:
        """
        Get text length.

        Returns:
            int: text length
        """
        return len(self.text)

    @property
    def gram_length(self) -> int:
        """
        Get gram length.

        Returns:
            int: gram length
        """
        return self.__gram_length

    @property
    def fitness(self) -> float:
        """
        Get statistical fitness of the text according to the gram length.

        Returns:
            float: fitness
        """
        return self.__fitness

    def index_coincidence(self) -> float:
        """
        Compute the index of coincidence of our text.

        Returns:
            float: index of coincidence
        """
        count_letters = [self.text.count(chr(ord("A") + i)) for i in range(26)]
        return sum(map(lambda n: n * (n - 1), count_letters)) / (
            self.length * (self.length - 1)
        )

    def frequencies(self, min: int = 1, precision: int = 3) -> dict:
        """
        Compute the frequencies of n-group of letters within a text.

        Args:
            n (int): size of group of letters

        Returns:
            dict: frequencies
        """
        total_count = 0
        for i in range(self.length - self.gram_length + 1):
            gram = self.text[i : i + self.gram_length]
            entry = [gram, 0, CryptanalysisFunc.frequency(self.text, gram), 0.0]
            total_count += entry[self.INDEX_FREQ]
            if entry[self.INDEX_FREQ] >= min:
                self.__freqs[gram] = entry
                if entry not in self.__most_used_grams:
                    self.__most_used_grams.append(entry)
        for entry in self.__freqs.values():
            entry[self.INDEX_RELATIVE_FREQ] = CryptanalysisFunc.relative_frequency(
                entry[self.INDEX_FREQ], total_count, precision
            )
            entry[self.INDEX_FITNESS] = CryptanalysisFunc.fitness(
                entry[self.INDEX_FREQ], self.length, self.gram_length, precision
            )
            self.__fitness += entry[self.INDEX_FITNESS]
        self.__most_used_grams.sort(key=lambda e: e[self.INDEX_FREQ], reverse=True)
        self.__most_used_grams = list(
            map(lambda e: e[self.INDEX_GRAM], self.__most_used_grams)
        )
        return self.__freqs, self.__most_used_grams

    def match_with_english_frequencies(self, limit: int = 0) -> dict:
        """
        Associate frequencies with the known english letters according to the frequency analysis.

        Returns:
            dict: matched text grams with english gram
        """
        en_freqs = getattr(self, f"EN_FREQS_{self.__gram_length}")
        if limit != 0:
            en_freqs = en_freqs[:limit]
        min_length = min(len(self.__most_used_grams), len(en_freqs))
        self.__dict_matches = {
            self.__most_used_grams[i]: en_freqs[i][self.INDEX_GRAM]
            for i in range(min_length)
        }
        return self.__dict_matches

    def top_matches(self, k: int) -> Tuple[dict, list]:
        """
        Get {k}-top matches of the most used grams.

        Args:
            k (int): top k matches to retrieve

        Returns:
            Tuple[dict, list]: top k matches as dict and most used k-grams
        """
        return {
            gram: self.__dict_matches[gram] for gram in self.__most_used_grams[:k]
        }, self.__most_used_grams[:k]

    def replace_text_by_matches(self) -> str:
        """
        Replace text by frequencies analysis result on each gram of the text.

        Returns:
            str: text replaced
        """
        text_copy = str(self.text)
        for gram in self.__dict_matches:
            text_copy = text_copy.replace(gram, self.__dict_matches[gram].lower())
        return text_copy

    def split_into_subgroups(self, n: int) -> list:
        """
        Split the text in {n} group such as every i%n letter are grouped together.

        Args:
            n (int): size of group

        Returns:
            list: groups of i%n letters
        """
        groups = [""] * n
        for i in range(self.length):
            groups[i % n] += self.text[i]
        return groups

    def count_patterns(self, n: int = 1, threshold: int = 1) -> dict:
        """
        Count every pattern and store it in a dictionnary if the count is equal or larger than the given {threshold}.

        Args:
            n (int, optional): pattern's size
            threshold (int, optional): count threshold

        Returns:
            dict: patterns and its count
        """
        result = {}
        for i in range(self.length - n + 1):
            pattern = self.text[i : i + n]
            count = self.text.count(pattern)
            if count >= threshold:
                result[pattern] = count
        return result

    def distance_patterns(self, n: int = 1) -> dict:
        """
        Calculate the maximum distanc between two equal pattern in the text.

        Args:
            n (int, optional): size of the pattern

        Returns:
            dict: distances of patterns
        """
        distances = {}
        for i in range(self.length - n + 1):
            pattern = self.text[i : i + n]
            count = self.text.count(pattern)
            if count > 1:
                occ1 = self.text.find(pattern)
                occ2 = self.text.find(pattern, occ1 + 1)
                distances[pattern] = occ2 - occ1
        return distances

    def kasiski_test(
        self, n: int = 3, threshold: int = 11, min_probability: float = 0.0
    ) -> list:
        """
        Process the Kasiski's test to find the key length of the text.

        Kasiski's test works only on regular Vigenere ciphered text.

        Args:
            n (int, optional): size of patterns
            threshold (int, optional): exclusive max size of key length
            min_probability (float, optional): minimun probability to consider for a potential key length

        Returns:
            list: list of key length and its probabilities
        """
        divs = []
        probabilities = []
        for i in range(n, threshold):
            patterns = self.distance_patterns(n=i)
            max_distance = max(patterns.values())
            divs.extend(
                [
                    x
                    for x in range(1, int(math.sqrt(max_distance)) + 1)
                    if max_distance % x == 0 and x > 1
                ]
            )
        for div in set(divs):
            probabilities.append((div, divs.count(div) / len(divs)))
        if min_probability > 0.0:
            probabilities = filter(lambda t: t[1] > min_probability, probabilities)
        return sorted(probabilities, reverse=True, key=lambda t: t[1])

    def find_key_by_fitness(self, key_length: int) -> str:
        """
        Find the key by applying the statistical attack of fitness in English.

        Args:
            key_length (int): key length used on the ciphered text

        Returns:
            str: key value
        """
        key = ""
        for k in range(key_length):
            fitnesses = []
            for i in range(26):
                fitnesses.append(
                    CryptanalysisFunc.attack_vigenere_by_fitness(
                        chr(ord("A") + i), self.text, key_length, k
                    )
                )
            key += max(fitnesses)[1]
        return key
