"""Module for classes designed for DAT510 assignment A1."""
from .sdes import *
from .polyalphabetic import *
from .exectimer import *