"""DAT510 assignment A1 part 1 on Polyalphabetic cipher."""
from lib import TextAnalyzer, CryptanalysisFunc, exectimer
from prettytable import PrettyTable
import matplotlib.pyplot as plt
import numpy as np

MAX_KEYLENGTH = 10
MAX_DISPLAY_LENGTH = 80
PROMPT_SUBCHOICE = "> What to do: "
PROMPT_CONTINUE = "> Press <ENTER> to continue...\n"

def plot_en():
    """Plot the english monograms frequencies."""
    with open("data/part1/en_monograms.csv") as fp:
        frequencies = []
        total = 0
        for line in fp:
            data = line.split()
            data[1] = int(data[1])
            total += data[1]
            frequencies.append(data)
        for f in frequencies:
            f[1] = (f[1] / total) * 100

        labels = list(map(lambda f: f[0],frequencies))
        frequencies = list(map(lambda f: round(f[1],2),frequencies))
        x = np.arange(len(frequencies)) 
        width = 0.5
        fig, ax = plt.subplots()
        rect = ax.bar(x, frequencies, width, label='Letters frequency', color="green")
        ax.set_ylabel('Frequencies by letter')
        ax.set_title('Frequencies letters in English')
        ax.set_xticks(x)
        ax.set_xticklabels(labels)
        ax.legend()
        ax.bar_label(rect, padding=5)
        fig.tight_layout()
        plt.show()
    return

def task_1(*args):
    """Task 1: crack the polyalphabetic ciphered text by finding the key size and the key."""

    def attack_with_unknown_keylength(anl: TextAnalyzer) -> list:
        return [anl.find_key_by_fitness(i) for i in range(1, MAX_KEYLENGTH + 1)]

    def attack_with_kasiski_keylength(
        anl: TextAnalyzer, min_probability: float = 0.0
    ) -> list:
        possible_keylength = anl.kasiski_test(min_probability=min_probability)
        return [
            anl.find_key_by_fitness(keylength[0]) for keylength in possible_keylength
        ]

    def attack_by_vigenere(
        anl: TextAnalyzer, keys: list, autokey=False, display_limit: int = -1
    ) -> bool:
        for key in keys:
            print(
                f"++ key='{key}': {CryptanalysisFunc.vigenere_shift(anl.text, key, autokey=autokey)[:display_limit]}"
            )
        return True

    def submenu_kasiski_test(anl: TextAnalyzer) -> list:
        possible_keylength = anl.kasiski_test(min_probability=0.0)
        print(f"+ Kasiski's test found: {possible_keylength}")
        print(
            f"+ It looks like there are 2 strong possible keys which are {possible_keylength[:2]}"
        )
        print(f"+ Let's add a second assumption:\n")
        print(f"ASSUMPTIONS:")
        print(
            "\t#1. The cipher used is Vigenere because it is one of the most popular Polyalphabetic cipher (and the one seen in class)."
        )
        print(
            f"\t#2. The key length is probably {possible_keylength[:2]} thanks to Kasiski's test.\n"
        )
        return possible_keylength

    def submenu_vigenere_attack(anl,choice=None) -> bool:
        print("Vigenere attack by frequency analysis on 1-gram as a fitness.")
        print("1. Use Kasiski's test to find the key length")
        print(f"2. Try all the key length possible from 1 up to {MAX_KEYLENGTH}\n")
        subchoice = input(PROMPT_SUBCHOICE) if not choice else choice
        if subchoice == "1":
            keys_found = attack_with_kasiski_keylength(anl, min_probability=0.1)
            do_autokey = False
        elif subchoice == "2":
            keys_found = attack_with_unknown_keylength(anl)
            do_autokey = True
        else:
            return
        print(f"+ Keys found: {keys_found}")
        print(f"+ Vigenere attack:")
        attack_by_vigenere(anl, keys_found, autokey=do_autokey, display_limit=50)
        return True

    def submenu_check_cipher_is_vigenere(anl: TextAnalyzer) -> bool:
        print("+ Checking if the ciphertext has been encrypted by Vigenere.")
        print("Using index of coincidence to match up...")
        with open("data/part1/english_raw_text.txt", "r") as fp:
            print(f"\tEnglish text: {TextAnalyzer.IC_ENGLISH}")
            print(f"\tCipher text: {anl.index_coincidence()}")
            english_text = fp.read().lower()
            textlength = len(english_text)
            for keytest in ["MYKEY", "MYKEYVG", "MYKEYVGCAHIQ"]:
                subanl = TextAnalyzer(
                    CryptanalysisFunc.vigenere_shift(english_text, keytest)
                )
                print(
                    f"\tVigenere text of size {textlength} with keylength {len(keytest)}: {subanl.index_coincidence()}"
                )
            print(
                f"+ It seems like Vigenere is not the cipher used according to the index of coincidence. Indeed, the key has to be longer than {MAX_KEYLENGTH} to be close to {anl.index_coincidence()}."
            )
            print(
                f"+ Unless it is Vigenere with an autokey system as we have seen it in lecture (book p.104), instead of a repeated key...\n"
            )
            print(f"ASSUMPTIONS:")
            print(
                "\t#1. The cipher used is Vigenere because it is one of the most popular Polyalphabetic cipher (and the one seen in class)."
            )
            print(f"\t#3. The cipher is Vigenere but with an autokey system.\n")
        return True

    with open("data/part1/task1.txt", "r") as fp:
        gram_length = 2
        anl = TextAnalyzer(fp.read(), gram_length=gram_length)
        print(
            "In order to have a start of point to crack this code, I've have to make an assumption.\n"
        )
        print("ASSUMPTIONS:")
        print(
            "\t#1. The cipher used is Vigenere because it is one of the most popular Polyalphabetic cipher (and the one seen in class)."
        )

        done = False
        while not done:
            print("1. Find the possible key length according to our assumptions #1.")
            print("2. Check if the cipher used is probably Vigenere.")
            print("3. Use Vigenere attack according to our assumption #3.\n")
            choice = input(PROMPT_SUBCHOICE) if not args[0] else args[0]
            if choice == "1":
                submenu_kasiski_test(anl)
            elif choice == "2":
                submenu_check_cipher_is_vigenere(anl)
            elif choice == "3":
                if len(args) == 2:
                    submenu_vigenere_attack(anl, args[1])
                else:
                    submenu_vigenere_attack(anl)
            else:
                return True
            if args[0]: exit(0)
            input(PROMPT_CONTINUE)
        return True


@exectimer.time_elapsed
def task_2(*args):
    """Task 2: Process the cracking of the "docs/part1/task1.txt" by using the Vigenere attack with frequency analysis while testing the time processing."""

    def displayer(results: list):
        """
        Display the result in a pretty table.

        Args:
            results (list): results

        Returns:
            (PrettyTable) displayer
        """
        displayer = PrettyTable()
        displayer.field_names = ["keylength", "key", "deciphered text"]
        displayer.add_rows(results)
        print(displayer)
        return displayer

    def shorten_string(string: str, limit: int):
        """
        Shorten a string to have a prettier display.

        Args:
            string (str): string to shorten
            limit (int): limitation of chars to display

        Returns:
            (str) shortened string
        """
        return f"{string[:limit]}..." if limit < len(string) else string

    with open("data/part1/task1.txt", "r") as fp:
        anl = TextAnalyzer(fp.read(), gram_length=1)
        keys_found = [anl.find_key_by_fitness(i) for i in range(1, MAX_KEYLENGTH + 1)]
        print(
            f"+ Keys found by frequency analysis with 1-gram as fitness: {keys_found}"
        )
        print(f"+ Vigenere attack with autokey system:")
        results = [
            (
                len(key),
                key,
                shorten_string(
                    CryptanalysisFunc.vigenere_shift(anl.text, key, autokey=True), MAX_DISPLAY_LENGTH
                ),
            )
            for key in keys_found
        ]
        displayer(results)
        return results


def task_3(*args):
    """Task 3: Use the algorithm of the Task 1 and explain why it does not work."""

    def displayer(results: list):
        """
        Display the result in a pretty table.

        Args:
            results (list): results

        Returns:
            (PrettyTable) displayer
        """
        displayer = PrettyTable()
        displayer.field_names = ["keylength", "key", "deciphered text"]
        displayer.add_rows(results)
        print(displayer)
        return displayer

    def shorten_string(string: str, limit: int):
        """
        Shorten a string to have a prettier display.

        Args:
            string (str): string to shorten
            limit (int): limitation of chars to display

        Returns:
            str: shortened string
        """
        return f"{string[:limit]}..." if limit < len(string) else string

    def crack_using_task_1_key(anl: TextAnalyzer) -> str:
        """
        Crack the text by using the key found in task 1 and the hint.

        Hint indicates that the text is cipher twice.

        Args:
            anl (TextAnalyzer): text analyzer

        Returns:
            str: text deciphered twice with Vigenere
        """
        print(f"+ Vigenere attack with autokey system:")
        key = "DATFBA"
        results = [
            (
                len(key),
                key,
                shorten_string(
                    CryptanalysisFunc.vigenere_shift(
                        CryptanalysisFunc.vigenere_shift(anl.text, key, autokey=True),
                        key,
                        autokey=True,
                    ),
                    MAX_DISPLAY_LENGTH,
                ),
            )
        ]
        displayer(results)
        return results

    def crack_without_knowing_key(anl: TextAnalyzer) -> list:
        """
        Crack the text without knowing the key found in task 1 using the hint and the same algorithm.

        Hint indicates that the text is cipher twice
        Algorithm in task 1 try choose the best key with fitness

        Args:
            anl (TextAnalyzer): text analyzer

        Returns:
            list: all possible deciphered text.
        """
        print(f"+ Keys found by frequency analysis with 1-gram as fitness")
        print(f"+ Vigenere attack with autokey system:")

        for i in range(1, MAX_KEYLENGTH + 1):
            key_found = anl.find_key_by_fitness(i)
            anl = TextAnalyzer(
                CryptanalysisFunc.vigenere_shift(anl.text, key_found, autokey=True),
                gram_length=1,
            )
            results = []
            for j in range(1, MAX_KEYLENGTH + 1):
                subkey_found = anl.find_key_by_fitness(j)
                results.append(
                    (
                        len(subkey_found),
                        subkey_found,
                        shorten_string(
                            CryptanalysisFunc.vigenere_shift(
                                anl.text, subkey_found, autokey=True
                            ),
                            MAX_DISPLAY_LENGTH,
                        ),
                    )
                )
            print(f"Iteration 1 with keylength of {i} and key '{key_found}'")
            displayer(results)
        print(
            "We can see that the key found in Task 1 is no more detected. This is because the statistical fitness calculated is erroneous by the second encryption layer."
        )
        return results

    with open("data/part1/task3.txt", "r") as fp:
        anl = TextAnalyzer(fp.read(), gram_length=1)
        print(
            "1. Crack the text without knowing the key used and the same algorithm in Task 1 with the given Hint."
        )
        print("2. Crack the text using the key found in Task 1 and the given Hint.\n")
        choice = input(PROMPT_SUBCHOICE) if not args[0] else args[0]
        if choice == "1":
            results = crack_without_knowing_key(anl)
        elif choice == "2":
            results = crack_using_task_1_key(anl)
        else:
            return
        return results
