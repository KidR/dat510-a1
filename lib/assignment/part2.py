"""DAT510 assignment A1 part 2 on SDES."""
from lib import SDES, TripleSDES, Utils, exectimer, TextAnalyzer
from prettytable import PrettyTable
from io import TextIOWrapper
import subprocess as proc
import re
import csv
import json
import os


@exectimer.time_elapsed
def task_1(*args):
    """Task 1: Use the implementation of SDES to complete the table of the assignment."""

    def display(fieldnames: list, results: list):
        """
        Display results in a prettier way.

        Args:
            results (list): list of result

        Returns:
            displayer
        """
        displayer = PrettyTable()
        displayer.field_names = fieldnames
        displayer.add_rows(map(dict.values, results))
        print(displayer)
        return displayer

    def build_data(key: str, text: str, is_encryption: bool) -> dict:
        """
        Build data according of is_encryption or not with SDES.

        Args:
            key (str): raw key
            text (str): text data either plaintext or ciphertext
            is_encryption (bool): encrypt if True else decrypt

        Returns:
            dict: data
        """
        return (
            {"key": key, "plain": text, "cipher": SDES.encrypt(key, text)}
            if bool(int(is_encryption))
            else {"key": key, "plain": SDES.decrypt(key, text), "cipher": text}
        )

    with open("./data/part2/task1.csv", "r", newline="") as fp:
        data = csv.DictReader(fp, delimiter=",")
        result = [
            build_data(
                key=line["key"], text=line["text"], is_encryption=line["is_encryption"]
            )
            for line in data
        ]
        print("* Task 1: SDES Cipher")
        display(["Key", "Plain", "Cipher"], result)
        return result


@exectimer.time_elapsed
def task_2(*args):
    """Task 2: Use the implementation of Triple SDES to complete the table of the assignment."""

    def display(fieldnames: list, results: list):
        """
        Display results in a prettier way.

        Args:
            results (list): list of result

        Returns:
            displayer
        """
        displayer = PrettyTable()
        displayer.field_names = fieldnames
        displayer.add_rows(map(dict.values, results))
        print(displayer)
        return displayer

    def build_data(k1: str, k2: str, text: str, is_encryption: bool) -> dict:
        """
        Build data according of is_encryption or not with TripleSDES.

        Args:
            k1 (str): raw key 1
            k2 (str): raw key 2
            text (str): text data either plaintext or ciphertext
            is_encryption (bool): encrypt if True else decrypt

        Returns:
            dict: data
        """
        return (
            {
                "k1": k1,
                "k2": k2,
                "plain": text,
                "cipher": TripleSDES.encrypt(k1, k2, text),
            }
            if bool(int(is_encryption))
            else {
                "k1": k1,
                "k2": k2,
                "plain": TripleSDES.decrypt(k1, k2, text),
                "cipher": text,
            }
        )

    with open("./data/part2/task2.csv", "r", newline="") as fp:
        data = csv.DictReader(fp, delimiter=",")
        result = [
            build_data(
                k1=line["k1"],
                k2=line["k2"],
                text=line["text"],
                is_encryption=line["is_encryption"],
            )
            for line in data
        ]
        print("* Task 2: TripleSDES Cipher")
        display(["Key 1", "Key 2", "Plain", "Cipher"], result)
        return result


def task_3(*args):
    """Task 3: crack SDES text ct1.txt and TripleSDES text ct2.txt."""

    def smart_bruteforce_sdes(
        blocks: list, outputfile: TextIOWrapper, nsample=8, regex=r"[a-zA-Z]"
    ):
        """
        Bruteforce SDES by testing all possible keys on a sample of blocks that fit the filters and store it into a jsonfile.

        Args:
            blocks (list): binary blocks
            outputfile (TextIOWrapper): output file descriptor
            nsample (int): sample size
            regex (regexp): matching filters

        Returns:
            dict: data
        """
        f_crack = lambda k, b: chr(int(SDES.decrypt(k, b), 2) % 128)
        best_ic = 0.0
        blocks = blocks[:nsample]
        result = {
            "k1": None,
            "plaintext": None,
            "nsample": nsample,
            "cipherblocks": blocks,
        }
        for i in range(2 ** SDES.KEY_LENGTH):
            is_valid = True
            text = ""
            k_block = 0
            while k_block < len(blocks) and is_valid:
                c = f_crack(Utils.binary(i, SDES.KEY_LENGTH), blocks[k_block])
                is_valid = re.match(regex, c)
                text += c
                k_block += 1
            if is_valid:
                current_ic = TextAnalyzer(text).index_coincidence()
                if current_ic == max(best_ic, current_ic):
                    best_ic = current_ic
                    result.update(
                        {"k1": Utils.binary(i, SDES.KEY_LENGTH), "plaintext": text}
                    )
        json.dump(result, outputfile, indent=4)
        return result

    def smart_bruteforce_triple_sdes(
        blocks: list, outputfile: TextIOWrapper, nsample=10, regex=r"[a-zA-Z]"
    ):
        """
        Bruteforce TripleSDES by testing all possible keys on a sample of blocks that fit the filters and store it into a jsonfile.

        Args:
            blocks (list): binary blocks
            outputfile (TextIOWrapper): output file descriptor
            nsample (int): sample size
            regex (regexp): matching filters

        Returns:
            dict:  possible keys and deciphered text
        """
        f_crack = lambda k1, k2, b: chr(int(TripleSDES.decrypt(k1, k2, b), 2) % 128)
        best_ic = 0.0
        blocks = blocks[:nsample]
        result = {
            "k1": None,
            "k2": None,
            "plaintext": None,
            "nsample": nsample,
            "cipherblocks": blocks,
        }
        for i in range(2 ** SDES.KEY_LENGTH):
            for j in range(2 ** SDES.KEY_LENGTH):
                is_valid = True
                text = ""
                k_block = 0
                while k_block < len(blocks) and is_valid:
                    c = f_crack(
                        Utils.binary(i, SDES.KEY_LENGTH),
                        Utils.binary(j, SDES.KEY_LENGTH),
                        blocks[k_block],
                    )
                    is_valid = re.match(regex, c)
                    text += c
                    k_block += 1
                if is_valid:
                    current_ic = TextAnalyzer(text).index_coincidence()
                    if current_ic == max(best_ic, current_ic):
                        best_ic = current_ic
                        result.update(
                            {
                                "k1": Utils.binary(i, SDES.KEY_LENGTH),
                                "k2": Utils.binary(j, SDES.KEY_LENGTH),
                                "plaintext": text,
                            }
                        )
        json.dump(result, outputfile, indent=4)
        return result

    def _run_cracking_process(func_cracking, inpath: str, outpath: str, nsample: int):
        with open(inpath, "r") as fp:
            data = fp.read()
            blocks = [
                data[SDES.TEXT_BLOCK_LENGHT * i : SDES.TEXT_BLOCK_LENGHT * (i + 1)]
                for i in range(len(data) // SDES.TEXT_BLOCK_LENGHT)
            ]
            with open(outpath, "w") as op:
                func_cracking(blocks, op, nsample=nsample)

    @exectimer.time_elapsed
    def run_cracking_sdes():
        print("+ Cracking SDES")
        _run_cracking_process(
            smart_bruteforce_sdes, "data/part2/ctx1.txt", "b3_sdes_out.json", 40
        )
        return

    @exectimer.time_elapsed
    def run_cracking_triplesdes():
        print("+ Cracking TripleSDES")
        _run_cracking_process(
            smart_bruteforce_triple_sdes,
            "data/part2/ctx2.txt",
            "b3_triplesdes_out.json",
            80,
        )
        return

    print("* Task 3: SDES and TripleSDES cracking")
    run_cracking_sdes()
    run_cracking_triplesdes()
    return True


def task_4(*args):
    """Task 4: run a webserver to use TripleSDES encryption and decryption."""
    try:
        os.environ["FLASK_APP"] = "server"
        return proc.check_output("flask run".split())
    except KeyboardInterrupt:
        print("\t*****Ending webserver.")
