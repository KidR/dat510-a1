<style>
  table{
    border: 1px solid black;
    text-align: center;
    table-layout: fixed;
  }
  th {
    background-color: #e0e0e0;
    color: black;
    border: 0px solid black;
  }
  td {
    border: 0px;
  }
</style>
<table>
  <tr><td style="text-align: left">Ly Rainui</td><td style="text-align: center;">DAT510 Assignment 1</td><td style="text-align: right">264553@uis.no</td></tr>
</table>


# Cryptanalysis of primitive ciphers



## Abstract

​	The purpose of this assignment is to apply the cryptography concepts. In the first part, we focus on developing a method to break an encrypted message which uses primitive ciphers, such as poly-alphabetics ciphers. In the second part, we implement a symmetric block cipher  inspired by the Data Encryption Standard (DES) which allows us to encrypt and decrypt a message. In addition, we will use the first part of the assignment as a basis for breaking a message using this cipher. Finally, we will introduce the next assignment by designing a primitive web service in which a user can try the decryption and encryption methods developed in the second part.

[TOC]

<div style="page-break-after: always;"></div>

## 1. Introduction

​	Cryptanalysis is a major field of Cryptography. In the 1900s, this process made it possible to win wars by deciphering coded information. We can quote Alan Turing who, during the Second World War, used this technique to decipher the messages coded by Enigma. Nowadays, cryptography has evolved and offers both symmetric and asymmetric encryption which involves more sophisticated cryptanalysis techniques.

In this project, we highlight cryptanalysis on a **poly-alphabetic cipher** and a simple implementation of a symmetric encryption with the **Simplified Data Encryption Standard** (SDES). We put into practice the concepts listed in *Cryptography and Network Security: Principles and Practice* [[1]](##References) to crack poly-alphabetic ciphers by using mathematical tools such as *frequecy analysis attack in English* [[3]](##References) or *N-grams language model* [[4],[5]](##References) and by implementing SDES [[6]](##References) in Python.

## 2. Design and Implementation

*Detail description of the design, procedure, and implementation of your project along with the following details from Part I and Part II.*

For the entire assignment, there is a main file `main.py` which resolve every tasks in an interactive way. Throughout this report, each task will be accompanied by the python main program execution commands to obtain the discussed results. From a programming point of view, all useful methods are written in the python module `lib/classes/` and the code to resolve the assignment is written in `lib/assignment`.

In order to use the python program in an interactive way, please run

```sh
$ python3 main.py
```

### 2.1. Poly-alphabetic Ciphers

​	This section focuses mainly on the poly-alphabetic cipher. Before starting the deciphering of the given text, I had to think about how I should proceed to find a solution to this problem. From the assignment text, we know that the key length is within the range of 1 and 10; however, we do not have any other information about the used cipher, except for the information about poly-alphabetic nature of the encryption. A simple search on Google shows that there are more than ten poly-alphabetic ciphers. It seems obvious to me that it is not reasonable to try every possible encryption algorithm to solve this task due to the lack of information.

#### 2.1.1. Assumption #1: Vigenère cipher

​	To give myself a starting point, I have made an initial assumption: **the cipher used in this task is probably the Vigenère Cipher**. In fact, a quick [search on wikipedia](https://en.wikipedia.org/wiki/poly-alphabetic_cipher) shows that Vigenère is the best-known example of poly-alphabetic cipher and it is also one of the ciphers that was discussed in the lectures.

We must find the keyword that was used to encrypt the ciphertext, but at this point, we still have no clue about how to proceed. The naive solution would be to use bruteforce to find the key. However, knowing that the key length is still a parameter that we do not have determined yet, it is easy to show the ineffectiveness of the bruteforce in this case.

For an unknown key size, the number of possible keys is expressed by the number of permutations in our alphabet $A$ which is the capital letters from A to Z.

$$
|K| = \sum_{i=1}^{10} |A|^{i} = \sum_{i=1}^{10} 26^{i} > 1.411671e^{14}
$$

<center><p style="font-size: 11px; text-align: center; font-style: italic">Equation 1. Number of possible keys with a key length between 1 and 10 in our alphabet.</p></center>

with $K$ the set of the possible keys for a size of $i \in [1,10]$.

As shown above, there is too many possible keys to consider with this approach, unless the key length is $|k| \leq 4$. It is even worse if it turns out that the Vigerère cipher is not the right cipher.

Instead, there is an elegant way to determine the key size by performing statistical analysis on the encoded text. This method called [Kasiski's Test](https://en.wikipedia.org/wiki/Kasiski_examination) has been created by a cryptographer named [Friedrich Kasiski](https://en.wikipedia.org/wiki/Friedrich_Kasiski) in 1863. It attacks poly-alphabetic substitution ciphers such as Vigenère.

#### 2.1.2. Assumption #2:  key length is either 5 or 7

​	The principle is based on the distances between consecutive occurrences of strings in a ciphered text. The distance is more likely to be multiples of the length of the keyword. In the example below, showing the comparison between the ciphertext and the plaintext, we notice that `abcde` appears twice and correspond to the encryption of the term `crypto`. The distance between these two occurrences is $20$. Kasiski's test mentions that there is a probability in which the key length belongs to the set of divisors of the distance found. Then, it is highly possible that the key length is $5$. For this test to be effective, it is necessary to calculate the distance for all sub-strings of size $n \geq 3$. The most common divisor among the calculated distances represents the most likely key size.

```plaintext
CIPHERTEXT: abcde ab cdeab cde abcde abcdeabcdeabc
PLAINTEXT:  crypto is short for cryptography
```

<center><p style="font-size: 11px; text-align: center; font-style: italic">Example E1.1 took from Wikipedia</p></center>

Once the Kasiski's test applied on the ciphertext of the Task 1, we obtained the following result:

| Key length | Probability of being the key length |
| :--------: | :---------------------------------: |
|     5      |                0.381                |
|     7      |                0.333                |
|     2      |                0.048                |
|     3      |                0.048                |
|     6      |                0.048                |
|     10     |                0.048                |
|     11     |                0.048                |
|     15     |                0.048                |

```shell
$ python3 main.py --task a1 --subtask 1
```

The table shows that we have two possible candidate keys to consider, which are $\forall k \in K', |k| = 5 \or |k| = 7$ with $K' \subset K$ the set of all possible key with the right key length $|k|$.

Thus, we can assume that the most likely key length used in current Vigenère cipher is either $|k| = 5$ or $|k| = 7$.


#### 2.1.3. Verify the assumption #1

​	At this stage, we have assumed that the cipher used in the Task 1 is Vigenère and, according to Kasiski's test, we have also determined that the key length is either $5$ or $7$. But we have no evidence that Vigenère is the algorithm used. To test this hypothesis, there are 2 ways to check it:

1. Knowing the key length, we can try all the possible keys with the length of 5 and the length of 7 if the first size is incorrect. We have to compute more than 800 million keys in the worst case or $\frac{|K'|}{2} = 4021845776$ keys in the average case:

$$
|K'| = \sum_{k \in {5,7}} 26^{k} = 8043691552 \\
$$

<center><p style="font-size: 11px; text-align: center; font-style: italic">Equation 2. Number of possible keys with a key length of 5 and 7 in our alphabet.</p></center>

2. Using a statistical metric known as **index of coincidence**. The computation of this index for a given text will give a probability value that can be compared to the index of coincidence of a language (i.e. $IC_{\text{English}} = 0.0667$):

$$
IC = \sum_{q \in A} \dfrac{n_{q}(n_{q}-1)}{n(n-1)}
$$

<center><p style="font-size: 11px; text-align: center; font-style: italic">Equation 3. Index of coincidence formula.</p></center>

with $q \in A$ every letters of our alphabet, $n_{q}$ the occurence of the letter $q \in A$ in the message and $n$ the total number of letters in the message.

Again, for evident reasons, we put aside the bruteforce solution. The interesting thing about the index of coincidence is its property of checking wether a text is probably written in english but also checking that a text is ciphered with Vigenère. Indeed, an english text ciphered with Vigenere has $IC \approx 0.045 \pm 0.05$ with a key length between $4$ and $8$. To proceed, we encrypt an english text (see [Appendix A](##Appendix-A)), which has a length close to our ciphertext, with Vigenère using different key sizes including those of our assumption #2. 

| Key length $|k|$ | Test key       | Index of coincidence |
| ---------------- | -------------- | -------------------- |
| 5                | `MYKEY`        | 0.04551              |
| 7                | `MYKEYVG`      | 0.04277              |
| 12               | `MYKEYVGCAHIQ` | 0.03927              |

```shell
$ python3 main.py --task a1 --subtask 2
```

All that remains is to compare them with the index of coincidence of our ciphertext:
$$
IC_{\text{Ciphertext}} = 0.03934
$$
<center><p style="font-size: 11px; text-align: center; font-style: italic">Equation 4. Index of coincidence of our ciphertext.</p></center>

With the results we have obtained, we conclude that Vigenère can not be the cipher used because $IC_{\text{Ciphertext}} \approx IC_{Vig_{k=12}} \approx 0.0393$ and it states that the key length can not be greater than $10$. Therefore, our assumption #1 and assumption #2 are no longer valid.

Although the index of coincidence tells us that Vigenère encryption cannot be used in our ciphertext, it also tells us something important: the index of coincidence of our ciphertext tends to approximate the index of coincidence of a random text: 
$$
IC_{Random} = \frac{1}{|A|} = \frac{1}{26} \approx 0.038 \\
IC_{Random} \approx IC_{\text{Ciphertext}}
$$
<center><p style="font-size: 11px; text-align: center; font-style: italic">Equation 5. Index of coincidence of a random text.</p></center>

This new information reveals that the encryption used has a random property. It appears that one of the random cipher known is an alternative of the Vigenère cipher called **Vigenère with Autokey System** which simulates randomness by replacing the use of repeating key with an Autokey  (see [[1]](##References)).

#### 2.1.4. Assumption #3: Vigenère Autokey System cipher

​	Before giving up my idea about Vigenère cipher, we still have to try the autokey system. As mentioned above, the autokey system is just a way to avoid repeated key so it does not change how the Vigenère cipher works. In fact, the autokey is composed of an initial key concataned with the plaintext. Once the Autokey is generated, we apply the regular Vigenère cipher. To illustrate how it works, let's take a look at the example E1.1.

```
PLAINTEXT : CRYPTOISSHORTFORCRYPTOGRAPHY
KEY       : MYKEY
AUTOKEY   : MYKEYCRYPTOISSHORTFORCRYPTOG
CIPHERTEXT: OPITRQZQHACZLXVFTKDDKQXPPIVE
```

By knowing the initial key $k$ used, we can build the autokey system by decoding the first $|k|$ letters of the ciphertext and add it as the continuation of our key. We repeat this pattern until we reach the end of the text.

To illustrate this, let's assume that we know the initial key $k = \text{MYKEY}$ and $|k| = 5$.

```
KEY       : MYKEY
CIPHERTEXT: OPITRQZQHACZLXVFTKDDKQXPPIVE
```
Apply Vigenère decipher on $5^{th}$ first letters of the text with the initial key `MYKEY` and update the autokey.
```
PLAINTEXT : CRYPT########################
AUTOKEY   : MYKEYCRYPT##################
```
Apply Vigenère decipher on next $5^{th}$ letters of the text with the found letters `CRYPT` and update the autokey.
```
PLAINTEXT : CRYPTOISSH######################
AUTOKEY   : MYKEYCRYPTOISSH#################
```
Repeat the process until the end of the text to find the autokey system used.
```
PLAINTEXT : CRYPTOISSHORTFORCRYPTOGRAPHY
AUTOKEY   : MYKEYCRYPTOISSHORTFORCRYPTOG
```

To accomplish this, we need **to determine the initial key** and a good option that allows us to find it is to use the **frequency analysis attack**.

#### 2.1.5. Frequency analysis attack

​	With the frequencies of letters in English (see Figure 1), it is possible to measure if a decoded text is written in english using the **N-grams log probability**  (see [[4],[5]](##References)). I have chosen to use the monogram frequency as a fitness mesure to conclude on the veracity of my decoded text.

![](../rsc/fig1-english_letters_frequencies.png)
<center><p style="font-size: 11px; text-align: center; font-style: italic">Figure 1. plotted with matplotlib in main.py using <a href="##References">[3]</a> dataset.</p></center>

The higher the log probability value obtained for a given text, the more it is likely that the text is in English. Unlike the index of coincidence, the log probability is more appropriate in this attack because the index of coincidence is calculated using the letters of a given text without any information on the language used, while the log probability is calculated by knowing the letters' frequency for a given language. For instance, let's say we used the index of coincidence in our frequency analysis attack and the highest value found is $IC = 0.07$. We are unable to deduce anything because the result is ambiguous.
$$
IC_{\text{English}} = 0.067; \space IC_{\text{Norsk}} = 0.069; \space IC_{\text{Danish}} = 0.07 \\
IC_{\text{Norsk}} \leq IC \leq IC_{\text{Danish}}
$$
To compute the log probability, we must extract all the monograms of our deciphered text, then multiply each of the monograms probabilities:
$$
\log(p(T)) = \sum_{i=1}^{|T|}\log(p(T_{i}))
$$
<center><p style="font-size: 11px; text-align: center; font-style: italic">Equation 6. Log probability of a monogram.</p></center>

with $p(T_{i})$ the log probability of the monogram at the position $i$ in the text $T$ calculate as follows:
$$
p(T_{i}) = \dfrac{f_{\text{English}}(T_{i})}{|A|}
$$
<center><p style="font-size: 11px; text-align: center; font-style: italic">Equation 7. Probability of a monogram.</p></center>

with $f_{\text{English}}(T_{i})$ the (relative) frequency of the monogram $T_{i}$ in the english language (Figure 1) and $|A|$ the total number of our alphabet since we are using monograms as fitness measure.

To find the initial key knowing its length, we can do as the following algorithm:

> 1. Create a default initial key `k = "AAAAA"`
> 2. For $i \in [1,|k|]$ do
>    2.1. For every letters in our alphabet $\forall a \in A$ do
>       2.1.1. Create a set of test key $S_{k}$ such as $k_{i}$ is replaced by $a$
>    2.2. Initial key $k$ becomes the test key in which its deciphered text has the best log probability
> 3. Return the initial key $k$

For an unknown length, we apply the same algorithm on every possible key length.

```sh
$ python3 main.py --task a2
```

Thanks to my assumptions, we decrypt the ciphertext by finding the key `DATFBA` (which represents DAT510 with `510` as indexes `FBA` ) with the Vigenère Autokey decipher algorithm in $0.281$ seconds.

> CRYPTOGRAPHYCANBESTRONGORWEAKCRYPTOGRAPHICSTRENGTHISMEASUREDINTHETIMEANDRESOURCESITWOULDREQUIRETORECOVERTHEPLAINTEXTTHERESULTOFSTRONGCRYPTOGRAPHYISCIPHERTEXTTHATISVERYDIFFICULTTODECIPHERWITHOUTPOSSESSIONOFTHEAPPROPRIATEDECODINGTOOLHOWDIFFICULTGIVENAL

However, if the ciphertext is using a second layer of encryption, the frequency analysis becomes useless. Indeed, when we try to use the index of coincidence and the log probability, the value will be biased because none of the decrypted text obtained will look like a piece of english text.

```shell
$ python3 main.py --task a3 --subtask 1
```

Unless, we are awared of the key used and that the encrypted text used a second encryption layer with the same key.

```sh
$ python3 main.py --task a3 --subtask 2
```

<div style="page-break-after: always;"></div>

### 2.2. Simplified DES

​	The Simplified DES is a block cipher for educational purpose which is inspired by the DES cipher and thus has similar properties and structure as DES. The implementation of the SDES followed the algorithm explained in [[6]](##References).

#### 2.2.1. SDES encryption and decryption

| Key        | Plain    | Cipher   |
| ---------- | -------- | -------- |
| 0000000000 | 00000000 | 11110000 |
| 0000011111 | 11111111 | 11100001 |
| 0010011111 | 11111100 | 10011101 |
| 0010011111 | 10100101 | 10010000 |
| 1111111111 | 11111111 | 00001111 |
| 0000011111 | 00000000 | 01000011 |
| 1000101110 | 00111000 | 00011100 |
| 1000101110 | 00001100 | 11000010 |

```sh
$ python3 main.py --task b1
```

#### 2.2.2. 3SDES encryption and decryption

| Key 1      | Key 2      | Plain    | Cipher   |
| ---------- | ---------- | -------- | -------- |
| 1000101110 | 0110101110 | 11010111 | 10111001 |
| 1000101110 | 0110101110 | 10101010 | 11100100 |
| 1111111111 | 1111111111 | 00000000 | 11101011 |
| 0000000000 | 0000000000 | 01010010 | 10000000 |
| 1000101110 | 0110101110 | 11111101 | 11100110 |
| 1011101111 | 0110101110 | 01001111 | 01010000 |
| 1111111111 | 1111111111 | 10101010 | 00000100 |
| 0000000000 | 0000000000 | 00000000 | 11110000 |

```sh
$ python3 main.py --task b2
```

#### 2.2.3. Cracking SDES and 3SDES

​	The strategy used to crack `ctx1.txt` and `ctx2.txt`, which are respectively encrypted by the SDES and 3SDES cipher, is to try every possible keys by using **bruteforce**. Contrary to the arguments set out in the [Poly-alphabetic ciphers](###2.1-Poly-alphabetic-Ciphers), the bruteforce is a great option because there is a few possible keys for a modern computer. In fact, the raw key length is $10$ and its bits value are $b \in \{0,1\}$ in both SDES and 3SDES.
$$
|K| = 2^{10} = 1024
$$
<center><p style="font-size: 11px; text-align: center; font-style: italic">Equation 8. Number of possible keys in SDES.</p></center>

$$
|K| \times |K| = 2^{20} = 1048576
$$
<center><p style="font-size: 11px; text-align: center; font-style: italic">Equation 9. Number of possible keys in 3SDES.</p></center>


Moreover, we can make **some improvements to bruteforce to reduce execution time**:

1. Reject a key when a deciphered character is not a letter. Indeed, each block of 8-bits in the content to crack refers to a character in the ASCII table and the plaintext message should only contains letters.
2. Decipher just a part of the text (e.g. 20 first characters) such as we have enough characters decrypted to determine if the value obtained looks like an english word.
3. Use the index of coincidence on the plaintext obtained to determine if the key tested is the right one. The key which has the highest index of coincidence such as it is close enough to the $IC_{\text{English}} = 0.0667$ is most likely the key used to encrypt.

By applying this strategy, we find the following result:

| Filename   | Key 1      | Key 2      | Execution time (seconds) |
| ---------- | ---------- | ---------- | ------------------------ |
| `ctx1.txt` | 1111101010 | -          | 0.175                    |
| `ctx2.txt` | 1111101010 | 0101011111 | 404.891                  |

```sh
$ python3 main.py --task b3
```

The result will be store in a json file called `b3_sdes_out.json` for SDES and `b3_triplesdes_out.json` for the 3SDES with the following format

```json
{
    "k1": "<best raw key found>",
    "plaintext": "<deciphered text according to the nsample>",
    "nsample": "<number of block we decipher instead of the whole text>",
    "cipherblocks": ["<1st block of 8-bits>",...,"<last block of 8-bits>"]
}
```

#### 2.2.4. 3SDES as a web service

​	We use the python framework [Flask](https://flask.palletsprojects.com/en/2.0.x/) to create a webserver. This webserver is the user interface to use our 3SDES implementation. In this task, the two raw keys are *hardcoded* in the webserver configuration `server.conf`. That way, the user can perform the 3SDES encryption and decryption by putting the text as an argument in the URL.

> http://127.0.0.1:5000/triplesdes?encrypt=<message as string>
> http://127.0.0.1:5000/triplesdes?decrypt=<3SDES blocks of 8-bits>

For instance, we can cipher the message `helloworld` by sending an HTTP GET request.

```sh
$ curl http://127.0.0.1:5000/triplesdes?encrypt=helloworld
<p>00010101010101111101110011011100010101001010101101010100101100111101110011010010</p>
```

and use the previous result to check if the decipher algorithm is working.

```sh
$ curl http://127.0.0.1:5000/triplesdes?decrypt=00010101010101111101110011011100010101001010101101010100101100111101110011010010
<p>helloworld</p>
```

The main drawbacks of this implementation of our webservice are:

- The raw keys are *hardcoded* in our server configuration. If a hacker get access to the webserver, the future communication using these keys are compromise.
- The raw keys of the server can be guess by a hacker. The hacker can cipher a simple message such as `abcdefgh` by trying all the possible keys (i.e. $1048576$ possible keys) and use  `/triplesdes?decrypt=` of the webservice. If the web service decipher correctly the encrypted text given by the hacker, then the hacker can conclude that the keys he used to cipher his message are the key used by the web server.

## 3. Test Results

​	This assignment has been done by using the *Test Driven Development* method, especially in the SDES part. Before implementing the core functions of the SDES cipher, the unit tests were written in the first place with the [pytest](https://docs.pytest.org/en/6.2.x/) framework. Thanks to this method, the resolution of the SDES part was done quickly and without problems.

To run the test, please run the following command:

```sh
$ pytest
```

By contrast, the first part required more thinking and research than development. The usage of the previous method was not relevant.

## 4. Conclusion

​	In conclusion, poly-alphabetic ciphers are mostly not robust against frequency analysis while block ciphers are susceptible to bruteforce depending on the size of the key and the complexity used (i.e., 3SDES). As shown in this report, we can use mathematic tools to determine various properties of a given ciphertext, such as checking if the given text is written in english or if the cipher encryption suspected is correct. But, the implemented solution for the first part of the assignment is mainly based on assumptions. That said, the code implemented is obsequious if the chosen cipher would have been different from Vigenère with Autokey. As a result, the cryptanalysis is hardly generic and depends very much on the type of data being processed (i.e., type of cipher, key length, etc...) and the expertise of the cryptanalyst. 

<div style="page-break-after: always;"></div>

## References

​	[1] Stallings, William. *Cryptography and Network Security: Principles and Practice*. 7th edition. Pearson Education, 2017. 

​	[2] Christensen, Chris. (2015). *Cryptanalysis of the Vigenère Cipher: The Friedman Test*. Retrieved September, 2021, from https://www.nku.edu/~christensen/1402%20Friedman%20test%202.pdf

​	[3] Pratical Cryptography. (n.d.). *English letters frequencies*. Retrieved September, 2021, from http://practicalcryptography.com/cryptanalysis/letter-frequencies-various-languages/english-letter-frequencies/

​	[4] Barter, Alex. (n.d.). *N-Gram Log Probability*. Retrieved September, 2021, from https://alexbarter.com/statistics/n-gram-log-probability/

​	[5] Jurafsky, Daniel  & Martin, James H. (2020). *N-gram Language Models*. Retrieved September 2021, from https://web.stanford.edu/~jurafsky/slp3/3.pdf

​	[6] Stallings,  William, *Appendix G: Simplified DES*. Retrieved September, 2021, from https://stavanger.instructure.com/courses/8818/files/1013682?wrap=1

<div style="page-break-after: always;"></div>

## Appendix

### Appendix A


> In our modern world, there are many factors that place the wellbeing of the planet in jeopardy. While some people have the opinion that environmental problems are just a natural occurrence, others believe that human beings have a huge impact on the environment. Regardless of your viewpoint, take into consideration the following factors that place our environment as well as the planet Earth danger. Global warming or climate change is a major contributing factor to environmental damage. Because of global warming

